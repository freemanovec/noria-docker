FROM rust:stretch

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y clang libclang-dev
RUN rustup default nightly
RUN git clone https://github.com/mit-pdos/noria
WORKDIR /noria
RUN cargo build --release --bin noria-server
ENTRYPOINT [ "cargo", "run", "--release", "--bin", "noria-server", "--" ]
